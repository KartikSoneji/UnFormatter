let
	TAB_SIZE = 4,
	COLUMNS = 0;

let code;

window.addEventListener("load", e => {
	code = document.querySelector("#Code");
	code.addEventListener("input", e => updateTextareaSize());
	
	code.addEventListener("keydown", e => {
		if(e.key == "Enter" && e.ctrlKey)
			unformatCodeInTextarea();
	});
	document.querySelector("#Unformat").addEventListener("click", e => unformatCodeInTextarea());
	
	document.querySelector("#TabSize").addEventListener("change", e => {
		TAB_SIZE = Math.max(1, parseInt(e.target.value));
		document.body.style.setProperty("--tab-size", TAB_SIZE);
	});
	document.querySelector("#Columns").addEventListener("change", e => COLUMNS = parseInt(e.target.value));
});

function updateTextareaSize(){
	let scrollY = window.scrollY;
	
	code.style.height = "auto";
	code.style.height = (code.scrollHeight) + "px";
	
	window.scrollTo(0, scrollY);
}

function unformatCodeInTextarea(){
	code.value = unformatCode(code.value);
	updateTextareaSize();
}

function unformatCode(rawCode){
	let code = rawCode.split("\n");
	endCharacters = [];
	
	let columns = Math.max(...code.map(e => textLength(e)), COLUMNS);
	for(let i = 0; i < code.length; i++){
		code[i] = code[i].trimEnd();
		
		endCharacters[i] = "";
		let newLine = "";
		for(let j = code[i].length - 1; j >= 0; j--)
			if(code[i][j].match(/[{};]/))
				endCharacters[i] = code[i][j] + endCharacters[i];
			else if(code[i][j].match(/[ \t]/))
				newLine = code[i][j] + newLine;
			else{
				newLine = code[i].substring(0, j + 1) + newLine;
				break;
			}
		
		code[i] = newLine.trimEnd();
	}
	
	for(let i = code.length - 1; i >= 1; i--)
		if(code[i].length == 0 && endCharacters[i].length != 0 && !code[i - 1].match(/(\/\/)|(\/\*)|(\*\/)/)){
			endCharacters[i - 1] += endCharacters[i];
			
			code.splice(i, 1);
			endCharacters.splice(i, 1);
		}
	
	for(let i = 0; i < code.length; i++)
		if(endCharacters[i].length > 0){
			code[i] += " ".repeat(columns - textLength(code[i]));
			code[i] += endCharacters[i];
		}
	
	return code.join("\n");
}

function textLength(text){
	let length = text.replace(/[{};]*$/g, "").length;
	for(let c of text)
		if(c == "\t")
			length += TAB_SIZE - 1;
	
	return length;
}