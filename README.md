# UnFormatter

Try it out: https://KartikSoneji.gitlab.io/UnFormatter.

## Inspired by
https://www.reddit.com/r/ProgrammerHumor/comments/6zpeaq/seek_help_if_you_do_this/
Credit: [`@hiscursedness`](https://twitter.com/hiscursedness/status/788690145822306304)
![Inspiration](Inspiration.png)